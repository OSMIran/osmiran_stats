#!/usr/bin/env python3

import os
import subprocess
import time
from pathlib import Path

import schedule

from src.logger.cus_logger import logger


def update_api():  # To be run every hour
    logger.info("Started API cache as subprocess")
    up_cache = subprocess.call(["src/most_active.py"])
    logger.info("Finished API cache as subprocess")


def down_and_store(): # To be run every 3 hours
    logger.info("Started gather.py as subprocess.")
    gather_retc = subprocess.call(["src/gather.py"])
    logger.info("Finished gather.py as subprocess.")

    logger.info("Started to_postgres as subprocess.")
    to_db_retc = subprocess.call(["src/to_postgres.py"])
    logger.info("Finished to_postgres as subprocess.")


schedule.every(3).hours.do(down_and_store)
schedule.every(1).hour.do(update_api)

if __name__ == "__main__":
    if Path.cwd().name != "osmiran_stats":
            raise ("Run program from root of repo dir.")
    os.environ['PYTHONPATH'] = '.'
    logger.info(f"Found database {os.getenv('POSTGRES_DB')} in env.")
    down_and_store()
    update_api()
    while True:
        schedule.run_pending()
        time.sleep(1)
