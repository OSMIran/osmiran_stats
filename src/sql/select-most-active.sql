SELECT
	username,
	count(username) AS all_count,

	-- Nodes
	count( -- Create Nodes
		CASE WHEN
			operation = 'create' AND type = 'node'
			THEN 1 ELSE NULL
		END) create_node,
    count( -- Modify Nodes
		CASE WHEN
			operation = 'modify' AND type = 'node'
			THEN 1 ELSE NULL
		END) modify_node,
	count( -- Delete Nodes
		CASE WHEN
			operation = 'delete' AND type = 'node'
			THEN 1 ELSE NULL
		END) delete_node,

	-- Ways
	count( -- Create Ways
		CASE WHEN
			operation = 'create' AND type = 'way'
			THEN 1 ELSE NULL
		END) create_way,
    count( -- Modify Ways
		CASE WHEN
			operation = 'modify' AND type = 'way'
			THEN 1 ELSE NULL
		END) modify_way,
	count( -- Delete Ways
		CASE WHEN
			operation = 'delete' AND type = 'way'
			THEN 1 ELSE NULL
		END) delete_way,

	-- Rels
	count( -- Create Rels
		CASE WHEN
			operation = 'create' AND type = 'relation'
			THEN 1 ELSE NULL
		END) create_rel,
    count( -- Modify Rels
		CASE WHEN
			operation = 'modify' AND type = 'relation'
			THEN 1 ELSE NULL
		END) modify_rel,
	count( -- Delete Rels
		CASE WHEN
			operation = 'delete' AND type = 'relation'
			THEN 1 ELSE NULL
		END) delete_rel

FROM
	changes
WHERE
	timestamp BETWEEN %s AND %s

GROUP BY
	username
ORDER BY
	all_count DESC
LIMIT 20;