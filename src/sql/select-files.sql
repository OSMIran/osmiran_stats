SELECT
	LPAD(ms_dir_num::text, 3, '0')  || '/' ||
	LPAD(directory_num::text, 3, '0') || '/' ||
	LPAD(file_num::text, 3, '0') || '.osc'
FROM
	files
WHERE
	added_to_db = 1