#!/usr/bin/env python3

from flask import Flask, request, jsonify

import json


app = Flask(__name__)

@app.route("/api/most-active", methods=["GET"])
def most_active():
    with open("assets/db/cache.json", "r") as json_file:
        return jsonify(json.load(json_file))

if __name__ == '__main__':
    app.run()

