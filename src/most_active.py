#!/usr/bin/env python3

from khayyam import JalaliDatetime, TehranTimezone

from datetime import timedelta, timezone
import json

from to_postgres import create_connection, db_config

def get_first_intervals():
    """Get first interval of jalali datetime to UTC
    :param:
    :return: beginning of each interval(day, week, month)
    """

    offset_hour = 4 if JalaliDatetime.now(TehranTimezone()).month <= 6 else 3
    teh_timezone = timezone(timedelta(hours = offset_hour, minutes = 30))
    utc_timezone = timezone(timedelta(0))
    jal_now = JalaliDatetime.now(teh_timezone)

    # Day Beginning
    bige_day = jal_now
    bige_day = bige_day.replace(hour = 0, minute = 0, second = 0, tzinfo = teh_timezone)
    end_day = bige_day + timedelta(days=1)
    bige_day_str = bige_day.todatetime().astimezone(tz = utc_timezone).strftime("%Y-%m-%d %H:%M:%S")
    end_day_str = end_day.todatetime().astimezone(tz=utc_timezone).strftime("%Y-%m-%d %H:%M:%S")

    # Week Beginning
    bige_week = jal_now
    while bige_week.isoweekday() != 1:
        bige_week -= timedelta(days=1)
    bige_week = bige_week.replace(hour=0, minute=0, second=0, tzinfo=teh_timezone)
    end_week = bige_week + timedelta(days=7)
    bige_week_str = bige_week.todatetime().astimezone(tz=utc_timezone).strftime("%Y-%m-%d %H:%M:%S")
    end_week_str = end_week.todatetime().astimezone(tz=utc_timezone).strftime("%Y-%m-%d %H:%M:%S")

    # Month Beginning
    bige_month = jal_now
    bige_month = bige_month.replace(day=1, hour=0, minute=0, second=0, tzinfo=teh_timezone)
    end_month = bige_month + timedelta(days=30)
    bige_month_str = bige_month.todatetime().astimezone(tz=utc_timezone).strftime("%Y-%m-%d %H:%M:%S")
    end_month_str = end_month.todatetime().astimezone(tz=utc_timezone).strftime("%Y-%m-%d %H:%M:%S")

    return {"bige_day": bige_day_str, "end_day": end_day_str,
            "bige_week": bige_week_str, "end_week": end_week_str,
            "bige_month": bige_month_str, "end_month": end_month_str}

def select_most_active(conn, timestamps):
    """query most active users in specific timestamp intervals
    :param conn: Connection object
    :param timestamp: Interval beginning of day, week and month
    :return res: Most active users
    """
    res_list = []
    entry_list = [[timestamps["bige_day"], timestamps["end_day"]],
                  [timestamps["bige_week"], timestamps["end_week"]],
                  [timestamps["bige_month"], timestamps["end_month"]]
    ]

    cur = conn.cursor()

    with open("src/sql/select-most-active.sql", "r") as select_active_sql:
        query_sql = select_active_sql.read()
        for entry in entry_list:
            cur.execute(query_sql, entry)
            res_list.append(cur.fetchall())
    return res_list

if __name__ == "__main__":
    conn_db = create_connection(db_config())
    results = select_most_active(conn_db, get_first_intervals())

    result_list = []
    for interval in results:
        interval_list = []
        for idx, user in enumerate(interval):
            user_dict = {"index": idx,
                         "username": user[0],
                         "all_changes": user[1],
                         "create_node": user[2],
                         "modify_node": user[3],
                         "delete_node": user[4],
                         "create_way": user[5],
                         "modify_way": user[6],
                         "delete_way": user[7],
                         "create_rel": user[8],
                         "modify_rel": user[9],
                         "delete_rel": user[10]
            }
            interval_list.append(user_dict)
        result_list.append(interval_list)

    res_json={"day_interval": result_list[0],
              "week_interval": result_list[1],
              "month_interval": result_list[2]
    }
    with open("assets/db/cache.json", "w") as json_file:
        json.dump(res_json, json_file)

    if (conn_db):
        conn_db.close()