#!/usr/bin/env python3

from logger.cus_logger import logger
import json
import concurrent.futures
import threading
from time import sleep
from pathlib import Path
import gzip
from io import BytesIO
import requests

from lxml.html import fromstring
import backoff
from lxml import etree as ET
from matplotlib import path as geo_path


def trim_OSC(xml_file, lim_border):
    """Trims OSC xml node, ways and relations with their lat/lon or bbox by geojson limit border.
    :param xml_file: parsed xml file to be trimmed
    :param lim_border: parsed geojson border file
    :returns: parsed trimmed xml var
    """
    try:
        # Create geo_path
        border_coords = lim_border["features"][0]["geometry"]["coordinates"]
        border_path = geo_path.Path(border_coords, closed=True)

        # Nodes
        nodes = xml_file.findall(".//node")
        nodes_info = [[float(node.attrib["lon"]), float(node.attrib["lat"])] for node in nodes]
        node_within_result = border_path.contains_points(nodes_info)
        for idx, node_el in enumerate(nodes):
            if not node_within_result[idx]:
                node_el.getparent().remove(node_el)

        # Ways and Relations (Areas that have bbox)
        areas = xml_file.findall(".//bbox")
        area_info = []
        for area in areas:
            minlat, maxlat = float(area.attrib['minlat']), float(area.attrib['maxlat'])
            minlon, maxlon = float(area.attrib['minlon']), float(area.attrib['maxlon'])
            area_info.append([minlon, minlat]); area_info.append([maxlon, minlat])
            area_info.append([maxlon, maxlat]); area_info.append([minlon, maxlat])
        area_within_result = border_path.contains_points(area_info)
        for idx, bbox_el in enumerate(areas):
            res_idx = ((idx+1)*4)-4
            if not True in area_within_result[res_idx:res_idx+4]:
                area_el = bbox_el.getparent()
                area_el.getparent().remove(area_el)

        # Remove empty elements
        for element in xml_file.xpath("/osmChange/*"):
            if not len(element.xpath("./*")):
                element.getparent().remove(element)

        #logger.info(f"Finished trimming {len(nodes)} node and {len(areas)} way and relation.")
    except Exception:
        logger.warning("Corrupted file.")
    return xml_file

@backoff.on_exception(backoff.expo,
                      (requests.exceptions.Timeout,
                       requests.exceptions.ConnectionError,
                       requests.exceptions.RequestException),
                       logger=logger)
def gen_folder_links(source_link):
    """Returns a list of folders from source
    :param source_link: the source url master folder
    :return: a list of folder links to be downloaded
    """
    downable_links = []

    # Get available folders
    #logger.info(f"Trying to request {source_link}.")
    master_page = requests.get(source_link)
    logger.info("Succesfully requested to master.")
    master_page_html = fromstring(master_page.text)
    folder_lists = master_page_html.xpath("/html/body/table/*")
    first_fol, last_fol = folder_lists[3].find("./td[2]/a").text[:3], folder_lists[-2].find("./td[2]/a").text[:3]

    for folder in range(int(first_fol), int(last_fol) + 1):
        downable_links.append(f"{source_link}/{folder:03}")
    #logger.info("Downlable folder links from master generated")
    return downable_links

@backoff.on_exception(backoff.expo,
                      (requests.exceptions.Timeout,
                       requests.exceptions.ConnectionError,
                       requests.exceptions.RequestException),
                       logger=logger)
def gen_file_links(fol_link):
    """
    Generate file urls form folders and write it to file
    :param fol_link: link of the requested folder
    :return:
    """
    # Downloaded files
    osc_path = cwd / "assets" / "trimmed-osc"
    downloaded = [str(d_file)[-11:-4] for d_file in osc_path.glob("**/*.osc")]

    #logger.info(f"Trying to request folder {fol_link[-3:]}.")
    folder_page = requ_ses.get(fol_link)
    logger.info(f"Succesfully requested to folder {fol_link[-3:]}.")
    folder_page_html = fromstring(folder_page.text)
    file_lists = folder_page_html.xpath("/html/body/table/*")
    first_fil, last_fil = file_lists[3].find("./td[2]/a").text[:3], file_lists[-2].find("./td[2]/a").text[:3]

    file_links = []
    for file in range(int(first_fil), int(last_fil) + 1):
        if f"{fol_link[-3:]}/{file:03}" not in downloaded:
            file_link = f"{source_link}/{fol_link[-3:]}/{file:03}.osc.gz"
            file_links.append(f"{file_link}\n")
    with open(f"assets/file_links/{fol_link[-3:]}", "w") as text_file:
        text_file.writelines(file_links)
    #logger.info(f"Download file links of {fol_link[-3:]} are generated.")

def uncompress_gzip(gzip_bin):
    """Returns xml parsed type from gzip binary request
    :param gzip_bin: binary format of compressed file
    :return: parsed xml uncompressed version"""
    with gzip.open(BytesIO(gzip_bin.content)) as f:
        xml_file = ET.parse(f)
    return xml_file

@backoff.on_exception(backoff.expo,
                      (requests.exceptions.Timeout,
                       requests.exceptions.ConnectionError,
                       requests.exceptions.RequestException),
                       logger=logger)
def download_link(downlink):
    """Download process of each file
    :param download: downlink of binary xml file
    : return: """

    #logger.info(f"Download request to {downlink[-14:-3]}")
    uncompressed = down_ses.get(downlink)

    sleep(1) # Mark would not let me off

    logger.info(f"Succesfully downloaded {downlink[-14:-3]}")

    parsed_xml = uncompress_gzip(uncompressed)

    file_dir = cwd/"assets"/"trimmed-osc"/downlink[-18:-11]
    try:
        file_dir.mkdir(parents=True, exist_ok=False)
    except FileExistsError:
        pass

    trimmed_osc = trim_OSC(parsed_xml, ir_limit_border)
    trimmed_osc.write(str(file_dir / downlink[-10:-3]),
                    pretty_print=True, xml_declaration=True, encoding="utf-8")
    logger.info(f"All done with {downlink[-14:-3]}.")

def get_session(thread_local):
    """Make a threadded session"""
    logger.info("Trying to make a threaded session")
    if not hasattr(thread_local, "session"):
        requ_session = requests.Session()
        requ_session.headers.update({"user-agent": agent})
        thread_local.session = requ_session
    logger.info("Done making a threaded session")
    return thread_local.session

def read_links(folders):
    """
    Read links from link files
    :param folders: string number of the folders
    :return: a list of all folders
    """

    #logger.debug("Reading urls from files...")
    all_links = []
    for file_url in [folder_num[-3:] for folder_num in folders]:
        with open(cwd / "assets" / "file_links" / file_url, "r") as f_url:
            for line in f_url.readlines():
                all_links.append(line.rstrip("\n"))
    #logger.debug("Finished reading urls from files.")
    return all_links


if __name__ == "__main__":
    if Path.cwd().name != "osmiran_stats":
        raise("Run program from root of repo dir.")

    cwd = Path.cwd()
    source_link = "https://download.openstreetmap.fr/replication/asia/minute/004"
    agent = 'Mozilla/5.0 (Linux; Android 8.0.0; SM-G960F Build/R16NW) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.84 Mobile Safari/537.36'

    # Parse GeoJSON
    with open(cwd/"assets"/"borders"/"ir-sim.geojson") as f:
        ir_limit_border = json.load(f)

    # Requests
    thread_local_red = threading.local()
    requ_ses = get_session(thread_local_red)

    folders = gen_folder_links(source_link)

    with concurrent.futures.ThreadPoolExecutor(max_workers=10) as req_executor:
        req_executor.map(gen_file_links, folders)


    # Downloads
    thread_local_down = threading.local()
    down_ses = get_session(thread_local_down)

    downs = read_links(folders)
    with concurrent.futures.ThreadPoolExecutor(max_workers=10) as down_executor:
        down_executor.map(download_link, downs)
    logger.debug("All done with gather.py")
