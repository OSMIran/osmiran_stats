# OSMIran Stats

![alt text](https://assets.gitlab-static.net/uploads/-/system/project/avatar/16359427/gitlab_stats.png "OSMStats Logo")

A simple program to gather, analyze and publish informations about OpenStreetMap changes of Iran.


## Installation
1. You can run rom srouce using `poetry`. ([read wiki](https://gitlab.com/OSMIran/osmiran_stats/-/wikis/Run-from-source))
2. Also, thanks to Gitlab CI use docker images. ([read wiki](https://gitlab.com/OSMIran/osmiran_stats/-/wikis/Run-using-docker))


## Contribute
In order to to contribute in this project:

- You can either make an issue and explain how this project can be improved.
- Edit the code directly and pull request.


## Workflow

```mermaid
graph TB

subgraph "gather.py"
  SubGraph1Flow("Planet OSM")
  fr_rep("OSM France Mirror")
  memory1("GZ File")
  memory2("GZ File")

  osc1("OSC File")
  osc2("OSC File")

  trim1("Trimmed Elements")
  trim2("Trimmed Elements")



  border{Border Polygon}

  SubGraph1Flow -- OSM Mirror --> fr_rep
  fr_rep -- thread1 --> memory1
  fr_rep -- thread2 --> memory2



  memory1 -. umcompress .-> osc1
  memory2 -. umcompress .-> osc2



  osc1 & border -.-> trim1
  osc2 & border -.-> trim2

end

subgraph "to_postgres.py"
  batch(Batch element)
  db[(Stats.db)]

  trim1 & trim2 -.-> batch
  batch --> db

end

subgraph "app.py"

  db -- query --> API

end
```


## Support this project 💰
Consider :star: this repo.

You also can donate to [Iran OSM](https://donate.osmiran.ir/) community.

## License
This project is licensed under GNU AFFERO GENERAL PUBLIC LICENSE v3.

